# Workflow for developers

**Don't make me think:** this is how we do develop, release and manage
software:

**Development:**

* [Design](#design)
* [DevOps platform](#gitlab-devops-platform)
* [Commit guidelines](#commit-guidelines)
* [New feature](#new-feature)
* [Bug fixes](#bug-fixes)
* [Merge Requests](#merge-requests)
* [Versioning](#versioning)
* [Release](#release)
* [Testing](#testing)
* [Code Quality](#code-quality)
* [Documentation](#documentation)
* [Dependency management](#dependency-management-python)
* [Environments](#environments)


## Design

Before you start developing a feature, make sure it is designed. If you are
working alone or in a small team, design can be implicit based on common
context and knowledge. If you are working in a team of more than 3 people,
implicit common context and knowledge are very difficult to maintain. In that
case it is almost always better to explicitly design a feature and share it for
review and feedback.

This can be as easy as explaining your design to a colleague. If you have
agreement, go ahead. If it's unclear you could:

* Get pencil and paper and sketch your design;
* Ask a third colleague;
* Back to the drawing board to rethink your design.

**Don't _over-design_, don't fall for _premature optimisation_.**


## Gitlab DevOps Platform

We use [gitlab](https://gitlab.com/satelligence) as our DevOps platform. All
software that we develop should be managed by git, and pushed to gitlab. Gitlab is
used to collaborate on Merge Requests (MR's) and for CI/CD. CI/CD includes automated
tests (unit tests, integration test), code quality checks (e.g. pylint, mypy), and
is used to manage deployment to staging or production.

If you develop something new, or create a bugfix, push your working branch to gitlab
regularly. Your work should not be on your laptop only.


### New projects

If you start a new repository you (or someone with the right permissions) must create
a new project on gitlab.

The easiest order is to start the gitlab project first, then clone that to your local
computer and start adding files. Once you have that local clone, it's easy to create
new branches, push things to gitlab, and create a MR.

When creating a new project, the limitation that any MR needs approval from another
developer *that has not committed to that MR*, is not automatically applied. For small
projects, where there are few developers, it can be too annoying or even impossible to
enforce that.
If at a certain moment it should be enforced, this can be enabled in the project's settings:
Settings->General->Merge request approvals. Tick "Prevent approvals by users who add commits".


## Commit guidelines

We use git for version control. Be nice to your colleagues and your future
self:

**Keep your commits atomic but small. Never rewrite history.**

* **Atomic** A commit should contain everything for the smallest functional
change. Every commit in master is valid and independent of future commits.
* **Small** The smaller a commit the better, big commits are harder to describe
and harder to comprehend.
* **Rewrite history** Rewriting history creates an array of issues in SCM. Never
apply force when pushing. Be gentle.

Tips:

Feel free to make one big mess out of you feature branch and even create a
merge request and ask for a review on your messy changes
(although clean commits are also easier to review).

But never change the history of commits available to team members. Use:
```
git checkout -b <name>_<feature>_nice_and_tidy
git log
```

You now have a new branch with the same messy commits. You have not pushed
this branch, so you can rewrite whatever you want. Copy the commit hash of the
commit before your mess.

```
git reset <hash>
```

You now have a clean history on a new branch with all your changes ready to
commit.

```
git add --patch <file containing code for multiple commits> <other files>
```

Using `--patch` or `-p` git gives an interactive view of all the changes in the
files. Add the changes for the first atomic commit according to the
instructions.

```
git diff --cached
```

To see all the changes staged for commit. If you are satisfied:

```
git commit
```

By not using `-a` you only commit the staged changes. By not using `-m` git
presents your default text editor. Describe your functional change:

* First line is 50 characters or less with a functional description of the
* change. Then a blank line Remaining text should be wrapped at 72 characters.
* Common format is to list your code changes with hyphens `-`.

Since you created an atomic commit, you may push your branch, create a merge
request and start over at `git checkout -b` to create a new branch for your
remaining changes. Alternatively add other atomic commits to the same merge
request by going back to `git add -p`.


## New feature

Pick an issue you'd like to develop from the TODOs. Check the DOD for
TODO >> Doing. If something is not clear, discuss with a colleague. Then,
start developing!::

    $ git checkout master
    $ git checkout -b <yourname>_<feature_name>


Check [git commit guidelines](#commit-guidelines).

When you are done developing (including tests and documentation):

* Update CHANGES.rst
* Check if feature is documented properly: code , system, user.
* Make (a) new ticket(s) for possible new issues / improvements.
* Merge with latest master:

```
$ git pull origin master
```

* Push the code to gitlab::

```
$ git push origin <yourname>_<feature_name>
```

* Open a pull request to master
* Ask someone to review your code
* Merge pull request with master
* Delete feature branch:

```
git branch -d <yourname>_<feature_name>
```

Move your issue to **Done**.


## Bug fixes

Fixing bugs has priority over creating new features. To fix a bug:

* Check which version is running on production: 3.1.3
* ``git checkout 3.1.3`` (you are now in detached mode)
* ``git checkout -b fixes_3.1`` (you are now a fixes branch)
* ``git checkout -b arjen_useful_bugfix_name``
* fix bug and make PR to fixes branch new release
* merge fix to fixes branch, release and deploy
* merge fixes branch to master

Step by step:

Create new fixes branch from ``tag`` and create new bugfix branch::

    $ git checkout <tag>
    $ git checkout -b <fixes_branch_name>
    $ git push origin <fixes_branch_name>
    $ git checkout -b <username>_<useful_bugfix_name>

Where <tag> is tagname eg ``1.4.4``. Write unit test for bug, fix bug,
document (including CHANGES.rst), commit, review then **release**, then merge
with master::

    $ git push origin <bugfix_branch_name>

* Open merge request to ``<fixes_branch_name>``.
* Ask someone to review your code.
* When ok, `release`_ bugfix **from fixes branch, make sure to only update the
  ``patch`` number**.
* Deploy to production.
* Merge fixes branch with master.

Delete bugfix branch::

    $ git branch -d <username>_<useful_bugfix_name>

Optionally you can choose to keep the fixes branch alive.

Do not linger your bugfixes around. It was a bug right? Otherwise you might
as well just put it in the normal feature flow. So build, release and deploy
it. 


## Merge Requests

Every change to the code should be made via a merge request in gitlab. **Never
push a fix or feature directly to master or a fixes branch!**

After you've pushed your new branch to gitlab, go to the project in gitlab and click
"merge requests". If you recently pushed a new branch, you'll be offered a blue button
to create a MR from that directly. Otherwise, click the "New Merge Request" button
and create a new MR from your branch.

Your MR will be started from a template. The templates are in the [s11-gitlab-templates]
(https://gitlab.com/satelligence/s11-gitlab-templates) project.

Provide a title that is short but a good summary of the MR.

Provide a concise and complete description of your MR: why was it done, what is
changed, and why, and what should be the result. This helps review, and serves as
an archive that documents our changes.

Until your MR is ready for review, prefix the title with "Draft:". This will prevent
accidental merge, and makes it clear to others that it is not ready yet. Once you think
it is ready for review, remove the "Draft" prefix and assign a reviewer.

Most of our projects require Approval from another developer before the MR can be merged.
Approval can be given by anyone **except the owner and other committers to the MR**. If approval
is given, and new commits are added, gitlab will remove the approval. All CI/CD pipelines
must have successfully finished before approval can be given.

The MR description contains a checkbox list for the reviewer. The reviewer should make sure
that all requirements as stated in this list are fulfilled and tick the boxes before approval.

The reviewer can start discussions, ask questions, or propose changes. For general
questions/discussions, use the overview section. For questions directly related to the 
code, add a comment in the changes section at the relevant code line by clicking in the
margin.

Once all discussions are resolved and the pipeline has ran to completion, the reviewer
must approve the MR. The MR can then be merged, either by the reviewer or by anyone else.


## Versioning

Software is versioned according to [semver](http://semver.org/) following a
MAJOR.MINOR.PATCH format. If the current version is 1.0.0: when development is
ready, features are released as 1.1.0 and deployed to staging environment. When
customer accepts the code on staging, the release may be deployed to production.

A bug on production is fixed on a bugfix branch tested and released as 1.1.1,
then deployed to production and merged to master. Major API breaking releases up
the major version.


## Release

### Python

Python packages are released with `fullrelease`.

#### Minor/major releases

To release *new features*, review and merge the merge request from master to the latest `fixes_X.Y`
branch. The merge will trigger a `Release automation` job that does the following:

- Create a release with zest.releaser:
  - On the `fixes_X.Y` branch, finalize CHANGES and setup.py version, and commit.
  - Tag with the release version
  - Generate new CHANGES section and dev version, and commit
- Merge back to `master`
- On master, change dev version in CHANGES and setup.py to next minor release
- Create next `fixes_X.Y` branch, where Y+=1
- Create next release MR, from `master` to the new `fixes_X.Y` branch.

#### Patch releases

The flow for a `*patch release* is similar, but not exactly equal. Whenever a MR with a fix is
merged to `fixes_X.Y`, a `Release automation` job is triggered automatically, that does this:

- Create a release with zest.releaser:
  - On the `fixes_X.Y` branch, finalize CHANGES and setup.py version, and commit.
  - Tag with the release version
  - Generate new CHANGES section and dev version, and commit
- From the just created release `X.Y.Z`, create a `patch_X.Y.Z` branch
- Create a MR from `patch_X.Y.Z` to `master`.

If there were already new major/minor versions released later than the fixes branch (e.g. the fix
was to fixes_3.60, but 3.61 was already released, and master is 3.62-dev), also (manually) create a
MR from the fixes branch that was just released to the fixes branch of the newer version, to make
sure the fixes are also included there. Once this MR is merged, a patch release for that branch
(e.g. `fixes_3.61`) will be created automatically too.

In SCM this looks like this::

    master
      |
      |   {user}_{feature}
      |----------
      |         |
      |     (features)
      |         |
      |   (pull request)
      |         |
      |<---------
      |
     1.0
      |
      |     {user}_{bugfix}
     1.1--------
      |         |
      |     (bugfix)
      |         |
      |   (pull request)
      |         |
      |     (release)
      |         |
      |       1.1.1
      |         |
      |<---------
      |

In the above example developers build features in feature branches from the
master branch.  After a pull request these are merged back into the master
branch.


### Docker

If you deploy your code as a container (Docker):

```bash
docker build -t registry.gitlab.com/satelligence/<repo-name>:base -f Dockerfile-base .
```

And push the base to the registry.

```bash
docker push registry.gitlab.com/satelligence/<repo-name>:base
```

To release a new version of classifier: tag and wait for Gitlab's CD to push a
new image to the registry:

```bash
git tag <major>.<minor>.<patch> - m "<release description>"
```


## Testing

All code should be tested. At least by an integration test. Preferably also
use unit tests. Unit and integration tests are run by a test suite and 
included in a Continuous Integration (CI) setup.

CI:
* Unit tests
* Integration tests

Workflow:
* Acceptance tests: on staging server; functional acceptance by data science;
* Load / Performance tests: on staging server;
* Stress tests: on staging server.

Load & performance testing is testing the system under production load (real
images, filled database). Stress testing is done to find the limits of the 
system.


## Code Quality

All code that ends up in production should adhere to certain quality standards. These
should be included in the CI/CD pipeline in gitlab. For python the recommended tools are
pylint, mypy and Codeclimate.


## Documentation

Documentation is done in Markdown or ReStructered Text and done at four levels:
* Code documentation (Python docstrings, enforced by Pylint in CI);
* System documentation (info on server setup and architecture;
  accessible through README);
* User documentation: how to use the software;
* Recovery documentation: how to get back up and running after disaster strikes.


## Dependency Management (python)

Dependency management (for python code) can be done in 2 places:

- requirements.txt (requirements_test.txt, etc)
- setup.py

All python code *must* have all necessary runtime dependencies declared in requirements.txt.
All dependencies in this file must be pinned to an exact version. This way every
developer can replicate the exact environment, avoiding hard-to-debug bugs due to
different dependency versions between developers.

Regular dependency version upgrades should be carried out. There is a monthly recurring
task in CU with a list of subtasks for that, one subtask per project. If a new project should be
included, add a subtask to this task.

Only python projects that should be pip-installable are required to also state their runtime
dependencies in setup.py. Contrary to the exact pinning in requirements.txt, in setup.py
package versions are preferably specified with a range. The lower bound is the lowest version
the code is known to work with. The upper bound is usually the next minor or major release
of the dependency, depending on their versioning scheme. This avoids sudden api changes that
could cause failures. Whenever a dependency has a new release that is higher than the upper
range, the range should be extended after testing compatibility (and reading the changelog).


## Environments

Separated development environments are used to prevent unstable code messing
up the production environment: Development Integration Staging Production (DISP)

* **Development** is your own machine or virtual machine, do whatever you
  want, configure as you like. Try to have a virtual machine or container on
  your local box that is configured as much as possible like the
  production environment. You probably run the unstable HEAD of your feature
  branch.
* **Integration** is an internal server used to integrate
  features. Integration is running the unstable HEAD of the **master** branch.
* **Staging** is an external accessible server with only released (pinned)
  packages, used to do acceptance tests. 
* **Production** is the production server. It runs a stable release.

Deployment to staging is done by developers, usually the one that did the release.
Deployment to production is done by OPS, once the tests on staging have succeeded.
For certain projects, we use "protected environments" in gitlab to enforce the above:
only devs can deploy to staging, only OPS can deploy to production.
To set up these protected environments, in gitlab go to settings->CI/CD->protected
environments (in addition to adding them to the relevant jobs in the .gitlab-ci.yml).
