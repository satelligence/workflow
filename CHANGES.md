Changelog of Workflow
=====================

- 2022-09-29: Updated release workflow to automated releases.

- 2022-09-12: Annual review. Updated `Release` section for new flow with protected main branches.

- 2022-09-09: added changelog.
